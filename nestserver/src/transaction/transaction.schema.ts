/* eslint-disable prettier/prettier */

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema() // Add ObjectType decorator
export class Transaction extends Document {
   // Add Field decorator for each property
  @Prop()
  amount: number;

  
  @Prop()
  sender: string;

  
  @Prop()
  receiver: string;

  
  @Prop({ required: true, default: Date.now })
  createdAt: Date;
}

export const TransactionSchema = SchemaFactory.createForClass(Transaction);
