/* eslint-disable prettier/prettier */

import { Field, InputType } from "@nestjs/graphql";

@InputType()
export class TransactionInput {

    
    @Field()
    readonly receiver: string;


    @Field()
    readonly  sender: string;

    @Field()
    readonly amount: number;


}





@InputType()
export class findTransactionInput {
    @Field()
    readonly _id: string;

}