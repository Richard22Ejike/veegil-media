/* eslint-disable prettier/prettier */
import { BadRequestException, Injectable, NotFoundException, UnauthorizedException, UseGuards } from '@nestjs/common';
import { Transaction } from './transaction.schema';
import { InjectModel } from '@nestjs/mongoose';
import { TransactionInput,  findTransactionInput,  } from './inputs/transaction.input';
import { Model, Types } from 'mongoose';
import { User } from 'src/user/user.schema';




@Injectable()
export class TransactionService {
    constructor(@InjectModel(Transaction.name) private readonly transactionModel: Model<Transaction>,
    @InjectModel(User.name) private readonly userModel: Model<User>
   ) {}

    

    async findAll(): Promise<Transaction[]> {
        return this.transactionModel.find().exec();
    }
    async create(createTransaction: TransactionInput): Promise<Transaction> {
      // Retrieve the sender and receiver accounts based on the provided IDs or any other unique identifiers
      const senderAccount = await this.userModel.findOne({ name: createTransaction.sender });
      const receiverAccount = await this.userModel.findOne({ name: createTransaction.receiver });
    
      // Check if both sender and receiver accounts exist
      if (!senderAccount || !receiverAccount) {
        throw new NotFoundException('Sender or receiver account not found');
      }
    
      // Check if the sender has sufficient balance for the transfer
      if (senderAccount.balance < createTransaction.amount) {
        throw new BadRequestException('Insufficient balance');
      }
    
      // Deduct the transferred amount from the sender's account
      senderAccount.balance -= createTransaction.amount;
    
      // Add the transferred amount to the receiver's account
      receiverAccount.balance += createTransaction.amount;
    
      // Create the transaction object with the provided properties
      const transaction = new this.transactionModel({
        amount: createTransaction.amount,
        sender: createTransaction.sender,
        receiver: createTransaction.receiver,
        createdAt: new Date(),
      });
    
      // Save the transaction
      await transaction.save();
    
      // Update the 'updatedAt' field of sender and receiver accounts with the current date
      const currentDate = new Date();
      senderAccount.UpdatedAt = currentDate;
      receiverAccount.UpdatedAt = currentDate;
    
      // Add the transaction to the sender's transactions list
      senderAccount.transactions.push(transaction);
    
      // Add the transaction to the receiver's transactions list
      receiverAccount.transactions.push(transaction);
    
      // Save the updated sender and receiver accounts
      await senderAccount.save();
      await receiverAccount.save();
    
      return transaction;
    }
    
  
    async findOne(user: findTransactionInput): Promise<Transaction> {
        return this.transactionModel.findById(user._id);
      }

   

      async deleteUser(_id: string): Promise<any>{
        return await this.transactionModel.deleteOne({_id: new Types.ObjectId(_id)});
      }
}
