/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { TransactionService } from './transaction.service';
import { findTransactionInput, TransactionInput} from './inputs/transaction.input';
import { TransactionDto } from './dto/transaction.dto';
import { Console } from 'console';


@Resolver()
export class TransactionResolver {
   constructor(
    private  transactionService: TransactionService,
   ) {}

   @Query(() => [TransactionDto])
   async getAllTransaction() {
    return this.transactionService.findAll();
   }
   @Mutation(() => TransactionDto)
   async createTransaction(@Args('input') input: TransactionInput) {
     const createdTransaction = await this.transactionService.create(input);
     
     // Map the created transaction to TransactionDto
     const transactionDto: TransactionDto = {
       _id: createdTransaction._id.toString(),
       receiver: createdTransaction.receiver,
       sender: createdTransaction.sender,
       amount: createdTransaction.amount,
       createdAt: createdTransaction.createdAt,
     };
     
     return transactionDto;
   }

   @Query(() =>TransactionDto)
   async findTransaction(@Args('input') input: findTransactionInput) {
    return this.transactionService.findOne(input);
   }

   @Mutation(() => String)
   async deleteTransaction(@Args('input') input: findTransactionInput): Promise<any> {
    await this.transactionService.deleteUser(input._id);
    return "Category Removed"
   }
}
