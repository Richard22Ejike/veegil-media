/* eslint-disable prettier/prettier */
import { Field, ObjectType } from "@nestjs/graphql";


@ObjectType()
export class TransactionDto {
    @Field()
    readonly _id: string;

    
    @Field()
    readonly receiver: string;


    @Field()
    readonly  sender: string;

    @Field()
    readonly amount: number;

    @Field()
    createdAt: Date;



  

  
  


  
}