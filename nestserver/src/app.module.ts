/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { UserModule } from './user/user.module';
import { TransactionModule } from './transaction/transaction.module';


@Module({
  imports: [  
    MongooseModule.forRoot('mongodb+srv://richardekene22:liverpool@cluster0.rymqwpm.mongodb.net/'),
    UserModule,
    TransactionModule,
  
    GraphQLModule.forRoot<ApolloDriverConfig>({
    driver: ApolloDriver,
    sortSchema: true,
    autoSchemaFile: 'src/schema.gql',
  }),],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
