/* eslint-disable prettier/prettier */
import { Injectable, UnauthorizedException, UseGuards } from '@nestjs/common';
import { User } from './user.schema';
import { InjectModel } from '@nestjs/mongoose';
import { UserInput, UpdateUserInput, findUserInput, SignInput } from './inputs/user.input';
import { Model, Types } from 'mongoose';
import { JwtService } from '@nestjs/jwt';
import { JwtAuthGuard } from 'src/jwt-auth.guard';



@Injectable()
export class UserService {
    constructor(@InjectModel(User.name) private readonly userModel: Model<User>
    ,private jwtService: JwtService) {}

    

    async signIn(signUser: SignInput): Promise<{ access_token: string, user: User } | undefined> {
      const user = await this.userModel.findOne({ name: signUser.name });
    
      if (!user || user.password !== signUser.password) {
        throw new UnauthorizedException('Invalid credentials');
      }
    
      const payload = { sub: user._id, username: user.name };
      return {
        access_token: await this.jwtService.signAsync(payload),
        user: user,
      };
    }

    async findAll(): Promise<User[]> {
        return this.userModel.find().exec();
    }

    async create(createUser: UserInput): Promise<User> {
      const User = new this.userModel(createUser);
      return User.save();
    }
  
    async findOne(user: findUserInput): Promise<User> {
        return this.userModel.findById(user._id);
      }
      @UseGuards(JwtAuthGuard)
    async update1(updateUser: UpdateUserInput): Promise<User> {
      const user = await this.userModel.findOne(new Types.ObjectId(updateUser._id));
      user.balance += updateUser.balance;
      user.UpdatedAt = new Date();
      return user.save();
      }
      @UseGuards(JwtAuthGuard)
      async update(updateUser: UpdateUserInput): Promise<User> {
        const user = await this.userModel.findOne(new Types.ObjectId(updateUser._id));
        user.balance -= updateUser.balance;
        user.UpdatedAt = new Date();
        return user.save();
        }

      async deleteUser(_id: string): Promise<any>{
        return await this.userModel.deleteOne({_id: new Types.ObjectId(_id)});
      }
}