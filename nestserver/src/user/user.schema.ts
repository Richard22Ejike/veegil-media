/* eslint-disable prettier/prettier */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document, Types } from 'mongoose';
import { Field, ObjectType } from '@nestjs/graphql';

@Schema()
@ObjectType() // Add ObjectType decorator
export class User extends Document {
  @Prop()
  name: string;

  @Prop()
  password: string;

  @Prop({ default: () => new Types.ObjectId().toHexString() })
  transactionId: string;

  @Prop({ required: true, default: () => new Number(0) })
  balance: number;

  @Prop({ required: true, default: () => new Date() })
  createdAt: Date;

  @Prop()
  UpdatedAt: Date;

 
  @Prop([
    {
      amount: { type: Number,  },
      sender: { type: String,  },
      receiver: { type: String,  },
      createdAt: { type: Date,  },
    },
  ])
  transactions: TransactionObject[];
}

export const UserSchema = SchemaFactory.createForClass(User);

@ObjectType()
class TransactionObject {
  @Prop()
  amount: number;

  
  @Prop()
  sender: string;

  
  @Prop()
  receiver: string;

  
  @Prop({ required: true, default: Date.now })
  createdAt: Date;
}
