/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UserService } from './user.service';
import { findUserInput, UserInput, UpdateUserInput, SignInput} from './inputs/user.input';
import { UserDto } from './dto/user.dto';
import { User } from './dto/usersign.dto';


@Resolver()
export class UserResolver {
   constructor(
    private  userService: UserService,
   ) {}

   @Query(() => [UserDto])
   async getAllUser() {
    return this.userService.findAll();
   }
   @Mutation(() => UserDto)
   async createUser(@Args('input') input: UserInput) {
    return this.userService.create(input);
   }
   @Mutation(() => UserDto)
   async signUser(@Args('input') input: SignInput) {
    return this.userService.signIn(input);
   }
   @Query(() => UserDto)
   async findUser(@Args('input') input: findUserInput) {
    return this.userService.findOne(input);
   }
   @Mutation(() => User)
   async updateUser(@Args('input') input: UpdateUserInput) {
    return this.userService.update(input);
   }

   @Mutation(() => User)
   async updateUser1(@Args('input') input: UpdateUserInput) {
    return this.userService.update1(input);
   }
   @Mutation(() => String)
   async deleteUser(@Args('input') input: findUserInput): Promise<any> {
    await this.userService.deleteUser(input._id);
    return "Category Removed"
   }
}
