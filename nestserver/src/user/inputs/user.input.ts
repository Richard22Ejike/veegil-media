/* eslint-disable prettier/prettier */

import { Field, InputType } from "@nestjs/graphql";

@InputType()
export class UserInput {
    @Field()
    readonly name: string;

    @Field()
    readonly password: string;


}

@InputType()
export class SignInput {
    @Field()
    readonly name: string;

    @Field()
    readonly password: string;

   

}


@InputType()
export class UpdateUserInput {
    @Field()
    readonly _id: string;

    @Field()
    readonly balance: number;
}

@InputType()
export class findUserInput {
    @Field()
    readonly _id: string;

}