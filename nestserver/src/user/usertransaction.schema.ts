/* eslint-disable prettier/prettier */
import { ObjectType, Field } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';


@Schema()
@ObjectType() // Add ObjectType decorator

export class userTransaction extends Document {
  @Field() // Add Field decorator for each property
  @Prop({ required: true })
  amount: number;

  @Field()
  @Prop({ required: true })
  sender: string;

  @Field()
  @Prop({ required: true })
  receiver: string;

  @Field()
  @Prop({ required: true, default: Date.now })
  createdAt: Date;
}

export const userTransactionSchema = SchemaFactory.createForClass(userTransaction);
