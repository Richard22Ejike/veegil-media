/* eslint-disable prettier/prettier */
import { Field, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class User {
    @Field()
    readonly _id: string;

    @Field()
    readonly name: string;

    @Field()
    readonly password: string;

    @Field()
    readonly transactionId: string;

    @Field()
    readonly balance: number;

    @Field()
    createdAt: Date;

    @Field({nullable:true})
    updatedAt: Date;

    @Field({nullable:true})
    access_token: string;

    @Field(() => [TransactionObject])
    transactions: TransactionObject[];
}

@ObjectType()
class TransactionObject {
  @Field()
  amount: number;

  @Field()
  sender: string;

  @Field()
  receiver: string;

  @Field()
  createdAt: Date;
}