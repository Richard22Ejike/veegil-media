/* eslint-disable prettier/prettier */
import { Field, ObjectType } from "@nestjs/graphql";
import { User } from "./usersign.dto";
import { userTransaction } from "../usertransaction.schema";

@ObjectType()
export class UserDto {
  @Field()
  readonly _id: string;

  @Field()
  readonly name: string;

  @Field()
  readonly password: string;

  @Field()
  readonly transactionId: string;

  @Field()
  readonly balance: number;

  @Field()
  createdAt: Date;

  @Field({ nullable: true })
  updatedAt: Date;

  @Field({ nullable: true })
  access_token: string;

  @Field(() => User)
  user: User;


}
