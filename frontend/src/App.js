import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  HttpLink,
  from,
} from "@apollo/client";
import { onError } from "@apollo/client/link/error";
import Form from "./components/form";
import GetUsers from "./components/gettransactions";
import { useState } from "react";
import Index from "./components/Index";


const errorLink = onError(({ graphqlErrors, networkError }) => {
  if (graphqlErrors) {
    graphqlErrors.map(({ message, location, path }) => {
      alert(`Graphql error ${message} ${location} ${path}`);

    });
  }
});




const link = from([
  errorLink,
  new HttpLink({ uri: "https://veegil-backend.onrender.com/graphql" }),
]);

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: link,
});

function App() {
  const [isLoggedIn, setLoggedIn ] = useState(false);
  return (
    <ApolloProvider client={client}>
      {" "}
       {/* <GetUsers />  */}
      <Index />
    </ApolloProvider>
  );
}
// function App() {
//   const [isLoggedIn, setLoggedIn ] = useState(false);

//   return (
//     <ApolloProvider client={client}>
//     <>
//       <Index isLoggedIn={isLoggedIn} setLoggedIn={setLoggedIn} />
//     </>
// </ApolloProvider>
//   );
// }

export default App;
