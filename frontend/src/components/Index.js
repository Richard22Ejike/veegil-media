



import SignIn from "./Auth/SignIn";
import SignUp from "./Auth/SignUp";
import Header from "./Navbar/Header";
import Dashboard from "./dashboard";
import VideoList from "./Video/VideoList";
import Video from "./Video/Video";
import {
    BrowserRouter,
    Routes,
    Route
} from "react-router-dom";
import  DrawerAppBar from"./Navbar/navbar"



export default function Index(props) {
  const { isLoggedIn, setIsLoggedIn } = props; // Fix the prop name here
  return (
    <div>

      <BrowserRouter>
     
          <Routes>
            <Route path="/" element={<SignIn setIsLoggedIn={setIsLoggedIn} isLoggedIn={isLoggedIn} />} />
            <Route path="/signup" element={<SignUp setIsLoggedIn={setIsLoggedIn} />} />
            <Route path="/hom" element={<DrawerAppBar setLoggedIn={setIsLoggedIn} />} />
            <Route path="/dashboard" element={<Dashboard setLoggedIn={setIsLoggedIn} />} />
            <Route path="/video" element={<VideoList setLoggedIn={setIsLoggedIn} />} />
            <Route path="/video/:id" element={<Video setLoggedIn={setIsLoggedIn} />} />
          </Routes>
    
      </BrowserRouter>
    </div>
  );
}

