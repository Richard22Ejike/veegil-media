import React, { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import { LOAD_USERS } from "../graphql/queries";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import CircularProgress from '@mui/material/CircularProgress';


export default function VideoList({ setLoggedIn }) {
    const { error, loading, data } = useQuery(LOAD_USERS);
    const [users, setUsers] = useState([]);
  
    useEffect(() => {
      if (data) {
        setUsers(data.getAllTransaction); // Update to data.getAllTransaction
      }
    }, [data]);
  
    if (loading) {
      return (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
          <CircularProgress />
        </div>
      );
    }
 
     
    if (error) {
      return <div>Error: {error.message}</div>;
    }

  
    return (
     <div>
      <h3 style={{ paddingLeft: '8px', marginLeft: '0.5cm' }}>All Transactions made on the site:</h3>
        <TableContainer component={Paper} sx={{ paddingLeft: '126px', paddingRight: '140px' }}>
        <Table sx={{ minWidth: 1050 }} size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>Made At</TableCell>
              <TableCell align="right">Amount</TableCell>
              <TableCell align="right">Receiver</TableCell>
              <TableCell align="right">Sender</TableCell>

            </TableRow>
          </TableHead>
          <TableBody>
            {users.map((val) => (
              <TableRow
                key={val.createdAt}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {val.createdAt}
                </TableCell>
                <TableCell align="right">{val.amount}</TableCell>
                <TableCell align="right">{val.receiver}</TableCell>
                <TableCell align="right">{val.sender}</TableCell>
          
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      </div>
    );

            }
