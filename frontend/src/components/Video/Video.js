import { UserContext } from '../UserContext';
import React, { useContext, useState, useEffect } from "react";
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useMutation } from "@apollo/client";
import { TRANSFER_MUTATION } from "../graphql/transferMutation";
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { DEPOSIT_MUTATION } from "../graphql/depositMutation";

const theme = createTheme();
export default function Transfer() {

  const [amount, setAmount] = useState("");
  const [receiver, setReceiver] = useState("");
  const [transferSuccess, setTransferSuccess] = useState(false);
  const [errorMessage, setErrorMessage] = React.useState('');
  const [createTransaction,] = useMutation(TRANSFER_MUTATION);
  const [updateUser1, { error, data }] = useMutation(DEPOSIT_MUTATION);
  const { userData } = useContext(UserContext);

  const { setUser } = useContext(UserContext);

  const addUser = () => {
    createTransaction({
      variables: {
        amount: parseFloat(amount),
        receiver: receiver,
        sender: userData.name,
      },
    })
      .then(() => {
        setTransferSuccess(true);
      })
      .catch((error) => {
        console.log(error);
      });
      updateUser1({
        variables: {
          balance: parseFloat('0'), 
          id: userData._id,
        },
      });
  };
  useEffect(() => {
    if (data && data.updateUser1 && data.updateUser1) {
      
      setUser(data.updateUser1); 
     
    } else if (error) {
      setErrorMessage('Sign-in failed. Please try again.'); // Update error message accordingly
  
    }
  }, [data, setUser, error]);

  if (!userData) {
    return <div>Loading...</div>; // Handle the case when user data is not available yet
  }

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="80" >
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            paddingLeft: 50,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Typography component="h1" variant="h5">
            Make Transfer
          </Typography>
          <Box component="form"
            onSubmit={addUser}
            noValidate sx={{ mt: 1 }}
          >
            <TextField
              type="text"
              required
              fullWidth
              placeholder="Recipient Account Number"
              name="Recipient Account Number"
              label="Recipient Account Number"
              id="Recipient Account Number"
              onChange={(e) => {
                setReceiver(e.target.value);
              }}
            />

            <TextField
              margin="normal"
              required
              fullWidth
              placeholder="Amount"
              name="Amount"
              label="Amount"
              type="Amount"
              id="Amount"
              onChange={(e) => {
                setAmount(e.target.value);
              }}
            />

            <Typography component="p" variant="p" color="red">
            </Typography>
            <Button
              onClick={addUser}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Transfer
            </Button>
          </Box>

          {transferSuccess && (
            <Typography component="p" variant="p" color="green">
              Transfer successful!
            </Typography>
          )}
        </Box>
      </Container>
    </ThemeProvider>
  );
}
