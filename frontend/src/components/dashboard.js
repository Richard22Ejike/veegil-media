import React, { useContext, useState, useEffect } from "react";
import { UserContext } from './UserContext';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { useMutation } from "@apollo/client";
import { DEPOSIT_MUTATION, WITHDRAW_MUTATION } from "./graphql/depositMutation";
import CircularProgress from '@mui/material/CircularProgress';

const Dashboard = () => {
  const [setErrorMessage] = React.useState('');
  const { setUser } = useContext(UserContext);
  const [balance, setbalance] = useState("");
  const [balances, setbalances] = useState("");
  const [updateUser1, { error: depositError, data: depositData}] = useMutation(DEPOSIT_MUTATION);
  const [updateUser, { error: withdrawError, data: withdrawData }] = useMutation(WITHDRAW_MUTATION);
  const { userData } = useContext(UserContext);

  const addUser = () => {
    updateUser1({
      variables: {
        balance: parseFloat(balance),
        id: userData._id,
      },
    });
  };

  const addUsers = () => {
    updateUser({
      variables: {
        balance: parseFloat(balances),
        id: userData._id,
      },
    });
  };

  useEffect(() => {
    if (depositData && depositData.updateUser1 && depositData.updateUser1) {
      setUser(depositData.updateUser1);
    } else if (depositError) {
      setErrorMessage('Sign-in failed. Please try again.'); // Update error message accordingly
    }
   
  }, [depositData, setUser, depositError]);

  useEffect(() => {
    
    if (withdrawData && withdrawData.updateUser && withdrawData.updateUser) {
      setUser(withdrawData.updateUser);
    } else if (withdrawError) {
      setErrorMessage('Sign-in failed. Please try again.'); // Update error message accordingly
    }
  }, [withdrawData, setUser, withdrawError]);

  if (!userData) {
    return (
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
        <CircularProgress />
      </div>
    );
  }

  const formattedCreatedAt = new Date(userData.createdAt).toLocaleString();
  const formattedUpdatedAt = userData.updatedAt
    ? new Date(userData.updatedAt).toLocaleString()
    : '';

  return (
    <div >
      <h2 style={{ paddingLeft: '8px', marginLeft: '0.5cm' }}>Welcome, {userData.name}!</h2>
      <div style={{ display: 'flex', gap: '16px', marginLeft: '0.5cm' }}>
        <Card variant="outlined" sx={{ maxWidth: 175 }}>
          <CardContent>
            Total Balance: <br />
            {userData.balance}
          </CardContent>
        </Card>
        <Card variant="outlined" sx={{ maxWidth: 175 }}>
          <CardContent>
            Account made at: <br />
            {formattedCreatedAt}
          </CardContent>
        </Card>
        <Card variant="outlined" sx={{ maxWidth: 175 }}>
          <CardContent>
            Last Transaction: <br />
            {formattedUpdatedAt}
          </CardContent>
        </Card>
      </div>
      <div style={{ display: 'flex',  paddingRight: 300,  }}>
       <Container component="main" maxWidth="80">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
          
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'left',
          }}
        >
          <Typography component="h1" variant="h5">
            Deposit
          </Typography>
          <Box component="form" onSubmit={addUser} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              placeholder="Amount"
              name="Amount"
              label="Amount"
              type="Amount"
              id="Amount"
              onChange={(e) => {
                setbalance(e.target.value);
              }}
            />
            <Typography component="p" variant="p" color="red"></Typography>
            <Button
              onClick={addUser}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Deposit
            </Button>
          </Box>
        </Box>
      </Container>

      <Container component="main" maxWidth="80">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
           
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'left',
          }}
        >
          <Typography component="h1" variant="h5">
            Withdraw
          </Typography>
          <Box component="form" onSubmit={addUsers} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              placeholder="Amount"
              name="Amount"
              label="Amount"
              type="Amount"
              id="Amount"
              onChange={(e) => {
                setbalances(e.target.value);
              }}
            />
            <Typography component="p" variant="p" color="red"></Typography>
            <Button
              onClick={addUsers}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Withdraw
            </Button>
          </Box>
        </Box>
       </Container>
      </div>
      
      <h3 style={{ paddingLeft: '8px', marginLeft: '0.5cm' }}>Transactions:</h3>
      <TableContainer component={Paper} sx={{ paddingLeft: '126px', paddingRight: '140px' }}>
        <Table sx={{ minWidth: 1050 }} size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>Made At</TableCell>
              <TableCell align="right">Amount</TableCell>
              <TableCell align="right">Receiver</TableCell>
              <TableCell align="right">Sender</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {userData.transactions.map((val) => (
              <TableRow
                key={val.createdAt}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {new Date(val.createdAt).toLocaleString()}
                </TableCell>
                <TableCell align="right">{val.amount}</TableCell>
                <TableCell align="right">{val.receiver}</TableCell>
                <TableCell align="right">{val.sender}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default Dashboard;
