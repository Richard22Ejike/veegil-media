import { gql } from "@apollo/client";

export const SIGN_UP_MUTATION = gql`
mutation CreateUser($name: String!, $password: String!) {
  createUser(input: { name: $name, password: $password }) {
    _id
    name
    password
    transactionId
    balance
    createdAt
    updatedAt
  }
}
`;
export const SIGN_IN_MUTATION = gql`
mutation signUser($name: String!, $password: String!){
    signUser(input: { name: $name, password: $name }) {
     access_token
     user{
       _id
      name
      password
      transactionId
      balance
      createdAt
      updatedAt
      transactions {
        amount
        sender
        receiver
        createdAt
      }
    }
    }
  }
  
`;
