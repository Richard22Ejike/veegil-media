import { gql } from "@apollo/client";

export const DEPOSIT_MUTATION = gql`
  mutation updateUser1($balance: Float!, $id: String!) {
    updateUser1(input: { balance: $balance, _id: $id }) {
      _id
      name
      password
      transactionId
      balance
      createdAt
      updatedAt
      transactions {
        amount
        sender
        receiver
        createdAt
      }
    }
  }
`;
export const WITHDRAW_MUTATION = gql`
  mutation updateUser($balance: Float!, $id: String!) {
    updateUser(input: { balance: $balance, _id: $id }) {
      _id
      name
      password
      transactionId
      balance
      createdAt
      updatedAt
      transactions {
        amount
        sender
        receiver
        createdAt
      }
    }
  }
`;