import { gql } from "@apollo/client";

export const TRANSFER_MUTATION = gql`
  mutation createTransaction($receiver: String!, $sender: String!, $amount:Float!) {
    createTransaction(input: { receiver: $receiver, sender: $sender, amount:$amount }) {

        amount
        createdAt
        receiver
        sender
        
      }
  }
`;
