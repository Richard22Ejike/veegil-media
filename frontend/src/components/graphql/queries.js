import { gql } from "@apollo/client";

export const LOAD_USERS = gql`
query {
    getAllTransaction{
      _id
      amount
      createdAt
      receiver
      sender
    }
  }
`;