import { gql } from "@apollo/client";

export const SIGN_IN_MUTATION = gql`
mutation signUser($name: String!, $password: String!){
    signUser(input: { name: $name, password: $password }) {
     access_token
     user{
       _id
      name
      password
      transactionId
      balance
      createdAt
      updatedAt
      transactions {
        amount
        sender
        receiver
        createdAt
      }
    }
    }
  }
  
`;