import React, {  useState,  useEffect } from "react";
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { SIGN_UP_MUTATION } from "../graphql/mutation";
import { useMutation } from "@apollo/client";

const theme = createTheme();
function Copyright(props) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}



export default function SignUp() {
  const [setErrorMessage] = React.useState('')
 

  const [name, setFirstName] = useState("");
  const [password, setPassword] = useState("");
  const [signUpSuccess, setSignUpSuccess] = React.useState(false);
  const [signUser, { error, data }] = useMutation(SIGN_UP_MUTATION);

  const addUser = (event) => {
    event.preventDefault();
    // Validate if the name is an 11-digit number using regex
    const isValidName = /^\d{11}$/.test(name);
    if (isValidName) {
      signUser({
        variables: {
          name: name,
          password: password,
        },
      });
    } else {
      setErrorMessage('Account Number should be a 11-digit number.');
    }
  };


  useEffect(() => {
    if (data ) { 
      setSignUpSuccess(true); 
    } else if (error) {
      setErrorMessage('Sign-in failed. Please try again.'); // Update error message accordingly
  
    }
  }, [data, error,]);

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <Box
            component="form"
            noValidate 
            onSubmit={addUser} 
            sx={{ mt: 3 }}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
              <TextField
        type="text"
        required
        fullWidth
        placeholder="Account Number"
        name="Account Number"
        label="Account Number"
        id="Account Number"
        onChange={(e) => {
          setFirstName(e.target.value);
        }}
      />
              </Grid>
              <Grid item xs={12}>
              <TextField
        margin="normal"
        required
        fullWidth
        placeholder="Password"
        name="password"
        label="Password"
        type="password"
        id="password"
        autoComplete="current-password"
        onChange={(e) => {
          setPassword(e.target.value);
        }}
      />
              </Grid>
        
            </Grid>
            <Button
              type="submit"
              onClick={addUser}
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign Up
            </Button>
            {signUpSuccess && (
          <Typography variant="body2" color="text.secondary" align="center" sx={{ mt: 2 }}>
            Sign Up Successful!
          </Typography>
             )}
            <Grid container justifyContent="flex-end">
              <Grid item>
                <Link href="/" variant="body2">
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{ mt: 5 }} />
      </Container>
    </ThemeProvider>
  );
}