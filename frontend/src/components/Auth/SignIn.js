import React, { useContext, useState, useEffect } from "react";
import { SIGN_IN_MUTATION } from "../graphql/mut";
import { useMutation } from "@apollo/client";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../UserContext";
import CircularProgress from '@mui/material/CircularProgress';

const theme = createTheme();

export default function SignIn(props) {
  const [errorMessage, setErrorMessage] = React.useState("");
  let navigate = useNavigate();
  const { setUser } = useContext(UserContext);
  const [name, setFirstName] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false); // State variable for loading state

  const [signUser, { error, data }] = useMutation(SIGN_IN_MUTATION);

  const addUser = () => {
    setLoading(true); // Set loading state to true
    signUser({
      variables: {
        name: name,
        password: password,
      },
    });
  };

  useEffect(() => {
    if (data && data.signUser && data.signUser.access_token) {
      localStorage.setItem("token", data.signUser.access_token);
      setUser(data.signUser.user);
      navigate("/hom");
    } else if (error) {
      setErrorMessage("Sign-in failed. Please try again."); // Update error message accordingly
    }
    setLoading(false); // Set loading state back to false
  }, [data, error, setUser, navigate]);

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}></Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box component="form" onSubmit={addUser} noValidate sx={{ mt: 1 }}>
            <TextField
              type="text"
              required
              fullWidth
              placeholder="Account Number"
              name="Account Number"
              label="Account Number"
              id="Account Number"
              onChange={(e) => {
                setFirstName(e.target.value);
              }}
            />

            <TextField
              margin="normal"
              required
              fullWidth
              placeholder="Password"
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
            <Typography component="p" variant="p" color="red"></Typography>

            {/* Conditionally render the button or the loader spinner */}
            {loading ? (
              // Loader spinner
              <Button fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>
                <CircularProgress size={24} color="primary" />
              </Button>
            ) : (
              // Sign-in button
              <Button
                onClick={addUser}
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Sign In
              </Button>
            )}

            <Grid container>
              <Grid item xs>
                <Link href="/Signup" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="SignUp" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}
